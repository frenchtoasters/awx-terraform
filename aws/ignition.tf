data "ignition_user" "rancher" {
    name = "rancher"
    home_dir = "/home/rancher/"
    shell = "/bin/bash"
    groups = ["docker"]
    ssh_authorized_keys = [aws_key_pair.rke-node-key.public_key]
}

data "ignition_systemd_unit" "dockerstart" {
  name = "busybox.service"
  enabled = true
  content = "[Unit]\nDescription=dockerstart\nAfter=docker.service network-online.target\nRequires=docker.service network-online.target\n\n[Service]\nTimeoutSec=0\nExecStartPre=-/usr/bin/docker kill busybox1\nExecStartPre=-/usr/bin/docker rm busybox1\nExecStartPre=/usr/bin/docker pull busybox\nExecStart=/usr/bin/docker run --name busybox1 busybox /bin/sh -c \"trap 'exit 0' INT TERM; while true; do echo Hello World; sleep 1; done\"\nRestart=Always\nRestartSec=10s\n\n[Install]\nWantedBy=multi-user.target"
}

# Ingnition config include the previous defined systemd unit data resource
data "ignition_config" "rancher" {
  systemd = [
    data.ignition_systemd_unit.dockerstart.rendered,
  ]
  users = [
    data.ignition_user.rancher.rendered,
  ]
}