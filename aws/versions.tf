terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    ignition = {
      source = "terraform-providers/ignition"
    }
    tls = {
      source = "hashicorp/tls"
    }
    rke = {
      source = "rancher/rke"
      version = "1.1.0"
    }
  }
  required_version = ">= 0.13"
}
