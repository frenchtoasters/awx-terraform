variable "region" {
  default = "us-east-1"
}

variable "instance_type" {
  default = "t2.medium"
}

variable "cluster_id" {
  default = "rke"
}

variable "ami_id" {
  default = "ami-007776654941e2586"
}

variable "aws_access_key" {
  default = ""
}

variable "aws_secret_access_key" {
  default = ""
}