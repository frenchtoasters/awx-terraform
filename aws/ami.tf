data "aws_ami" "flatcar" {
  most_recent = true

  filter {
    name   = "name"
    values = ["Flatcar-stable-2345.3.0-hvm"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["075585003325"] # Flatcar
}