module "nodes" {
    source = "./aws"
    region = "us-east-1"
    instance_type = "t2.medium"
    cluster_id = "rke"
    aws_access_key = var.aws_access_key
    aws_secret_access_key = var.aws_secret_access_key
}

resource "local_file" "cluster_key" {
    filename = "${path.root}/clusterkey.pem"
    content = module.nodes.private_key
}

provider "rke" {
    debug = false
    log_file = "${path.root}/rke.log"
}

resource "rke_cluster" "awx" {
    cloud_provider {
        name = "aws"
    }

    nodes {
        address = module.nodes.addresses[0]
        internal_address = module.nodes.internal_ips[0]
        user = module.nodes.ssh_username
        role = ["controlplane", "worker", "etcd"]
        ssh_key = module.nodes.private_key
    }

    network {
        plugin = "calico"
    }
    addon_job_timeout = 120
    addons_include = [
        "https://raw.githubusercontent.com/ansible/awx-operator/devel/deploy/awx-operator.yaml"
    ]
}

provider "kubernetes" {
  load_config_file = "false"
  insecure = "true"
  host     = rke_cluster.awx.api_server_url
  username = rke_cluster.awx.kube_admin_user

  client_certificate     = rke_cluster.awx.client_cert
  client_key             = rke_cluster.awx.client_key
  #cluster_ca_certificate = rke_cluster.awx.ca_crt
}

resource "kubernetes_namespace" "ansible-awx" {
  metadata {
    name = "ansible-awx"
  }
}

resource "local_file" "kube_cluster_yaml" {
    filename = "${path.root}/kube_config_cluster.yml"
    content  = rke_cluster.awx.kube_config_yaml
}

resource "local_file" "node_pem" {
  content = module.nodes.private_key
  filename = "${path.root}/node.pem"
}